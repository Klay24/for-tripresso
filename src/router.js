import Vue from 'vue'
import VueRouter from 'vue-router'
import TourList from './components/TourList.vue'

Vue.use(VueRouter)
export default new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
        name: 'TourList',
        path: '/',
        component: TourList,
    }, ]
})